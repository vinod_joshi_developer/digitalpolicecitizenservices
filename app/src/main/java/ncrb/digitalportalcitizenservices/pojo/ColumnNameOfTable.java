package ncrb.digitalportalcitizenservices.pojo;


public class ColumnNameOfTable {

    //user
    public static String LOGIN_ID ="LOGIN_ID";
    public static String LOGIN_PASSWORD ="LOGIN_PASSWORD";
    public static String PS_STAFF_CD ="PS_STAFF_CD";
    public static String OTP ="OTP";
    public static String TIME_STAMP ="TIME_STAMP";
    public static String TIME_OF_MOBILE ="TIME_OF_MOBILE";


    //fir
    public static String FIR_EVID_NUM ="FIR_EVID_NUM";
    public static String FIR_REG_NUM = "FIR_REG_NUM";
    public static String MEDIA_PATH = "MEDIA_PATH";
    public static String FIR_SRNO = "FIR_SRNO";

    //media
    public static String LATITUDE = "LATITUDE";
    public static String LONGITUDE = "LONGITUDE";
    public static String MEDIA_CHECKSUM = "MEDIA_CHECKSUM";
    public static String REG_YEAR = "REG_YEAR";
    public static String REG_DT = "REG_DT";

    public static String PS = "PS";
    public static String DISTRICT = "DISTRICT";






}// end main class
