package ncrb.digitalportalcitizenservices.pojo;


public class ServerSessionPoJo {

    public String server_user;
    public String server_pass;
    public String private_key;

    public String STATUS;
    public String STATUS_CODE;
    public String TOKEN;

    public String getTOKEN() {
        return TOKEN;
    }

    public void setTOKEN(String TOKEN) {
        this.TOKEN = TOKEN;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }

    public String getServer_user() {
        return server_user;
    }

    public void setServer_user(String server_user) {
        this.server_user = server_user;
    }

    public String getServer_pass() {
        return server_pass;
    }

    public void setServer_pass(String server_pass) {
        this.server_pass = server_pass;
    }
}// end main class
