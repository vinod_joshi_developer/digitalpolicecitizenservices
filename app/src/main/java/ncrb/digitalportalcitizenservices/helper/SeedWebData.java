package ncrb.digitalportalcitizenservices.helper;

import android.util.Base64;
import com.google.gson.Gson;
import java.util.Map;

import mha.ncrb.evidence.helper.CommonKotlin;

public class SeedWebData {

    public static String seedDataParam(Map<String, String> objMap) {

        Gson gson = new Gson();
        String jsonString = gson.toJson(objMap);

        CommonKotlin.Companion.logging("jsonString: "+jsonString);

        String baseStr = Base64.encodeToString(jsonString.getBytes(), Base64.DEFAULT);

        //encode data using special key
        //String baseStr = MCoCoRy.SeedData(jsonString);

        CommonKotlin.Companion.logging("baseStr: "+baseStr);

        return baseStr;

    }// seeddataparam

}// end main
