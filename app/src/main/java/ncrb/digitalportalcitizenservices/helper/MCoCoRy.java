package ncrb.digitalportalcitizenservices.helper;

import com.google.gson.Gson;

import org.apache.commons.codec.binary.Base64;

import java.security.Key;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import mha.ncrb.evidence.helper.CommonKotlin;
import ncrb.digitalportalcitizenservices.pojo.ServerSessionPoJo;

public class MCoCoRy {

    static {
        System.loadLibrary("native-lib");
    }

    public static native String getCredits();

    public static String SeedData(String webServerDataString) {

        String seed = null;
        try {
            Cipher cipher = createCipher(true);
            byte[] encryptedVal = cipher.doFinal(webServerDataString.getBytes());
            seed = new String(new Base64().encode(encryptedVal));
            CommonKotlin.Companion.logging("base64: seed:" + seed);
        } catch (Throwable e) {
            if (CommonKotlin.Companion.is_debug()) e.printStackTrace();
        }
        return seed;
    }

    public static String UnSeedData(String webServerDataString) {

        byte[] unseed = null;
        try {
            Cipher cipher = createCipher(false);
            unseed = cipher.doFinal(new Base64().decodeBase64(webServerDataString.getBytes()));
            CommonKotlin.Companion.logging("unseed:" + new String(unseed));
        } catch (Throwable e) {
            if (CommonKotlin.Companion.is_debug()) e.printStackTrace();
        }
        return new String(unseed);
    }

    private static Cipher createCipher(boolean encryptMode) throws Exception {

        int mode = encryptMode ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE;

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

        //convert base64 to objects to get private key
        String serverCreditsBase64 = new String(new Base64().decodeBase64(getCredits().getBytes()));
        ServerSessionPoJo serverCredits = new Gson().fromJson(serverCreditsBase64, ServerSessionPoJo.class);

        CommonKotlin.Companion.logging("serverCredits.private_key: "+serverCredits.private_key);

        char[] coco_private_key = serverCredits.private_key.toCharArray();

        byte[] salt = new byte[8];
        KeySpec spec = new PBEKeySpec(coco_private_key, salt, 65536, 128);
        SecretKey secretKey = factory.generateSecret(spec);
        byte[] encoded = secretKey.getEncoded();
        Key key = new SecretKeySpec(encoded, "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[16];
        cipher.init(mode, key, new IvParameterSpec(iv));

        return cipher;
    }

}// end main class
