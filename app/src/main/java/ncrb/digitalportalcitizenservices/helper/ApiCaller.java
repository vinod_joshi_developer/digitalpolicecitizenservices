package ncrb.digitalportalcitizenservices.helper;

import java.util.Map;

import ncrb.digitalportalcitizenservices.pojo.ServerSessionPoJo;
import ncrb.digitalportalcitizenservices.pojo.UserRequestPoJo;
import ncrb.digitalportalcitizenservices.pojo.UserResponsePoJo;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiCaller {

    @POST("auth/token")
    Call<ServerSessionPoJo> apiSessionToken();

    @POST("userservice/user/detail")
    Call<UserResponsePoJo> apiLogin(@Body UserRequestPoJo userRequestPoJo);


}// end api caller
