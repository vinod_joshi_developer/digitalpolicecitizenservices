package ncrb.digitalportalcitizenservices.helper;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mha.ncrb.evidence.helper.CommonKotlin;


/**
 * Class to hold all application utility methods
 *
 * @author Ultron
 */

public class CommonNCRB {

    private static final String TAG = "Utils";
    public static final String NCRB_DATE_FORMAT = "dd:MM:yyyy HH:mm:ss";

    // debug boolean enable or disable console log printing
    private static boolean DEBUG = true;

    /**
     * Constructor to prevent instantiation.
     */
    private CommonNCRB() {
        // Nothing to do
    }

    public static String getCurrentDate() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(NCRB_DATE_FORMAT, Locale.getDefault());
            return sdf.format(new Date());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return "00:00:0000 00:00:00";
    }

    /**
     * @param
     * @return
     */

    public static String getDateTime(String format) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String date = simpleDateFormat.format(new Date());
        printv(date);

        return date;
    }
    /**
     * @param
     * @return
     */

    public static String convertToDateFormat(Context context, String new_format_value
            , String existing_format_value, String date_value) {

        Date eDDte;
        try {
            // @Logic : convert string into date value - specially for MySQL
            SimpleDateFormat new_format = new SimpleDateFormat(new_format_value);
            SimpleDateFormat existing_format = new SimpleDateFormat(existing_format_value);
            eDDte = existing_format.parse(date_value);
            return new_format.format(eDDte);
        } catch (ParseException e) {
            if(CommonKotlin.Companion.is_debug()) e.printStackTrace();
        }
        return "";
    }// end showToastMsg

    /**
     * @param
     * @return
     */

    public static Date convertStrToDate(Context context, String format, String date_value) {

        // @Logic : convert string into date value - specially for MySQL
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            Date date_final = formatter.parse(date_value);
            return  date_final;
        } catch (ParseException e) {
            if(CommonKotlin.Companion.is_debug()) e.printStackTrace();
        }
        return new Date();

    }// end showToastMsg

    /**
     * @param
     * @return
     */

    public static void showToastMsg(Context context, String text, int toastDuration) {
        Toast.makeText(context, text, toastDuration).show();
    }// end showToastMsg

    /**
     * @
     */
    public static void printv(String printable) {
        if(DEBUG) System.out.println(printable);
    }

    /**
     * @
     */

    public static boolean isNumeric(String str) {

        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();

    }// end is numeric

    /**
     * @
     */

    public static void showAlert(Context context , String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }//  end show alert






    public static String parseDate(String date){
        String reformattedStr="Info Not Available";
        if(date!=null && date.length()>0){


        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date1 = inputFormat.parse(date);
            reformattedStr= outputFormat.format(date1);
        } catch (ParseException e) {
            long millisecond = Long.parseLong(date);
            // or you already have long value of date, use this instead of milliseconds variable.
            reformattedStr = DateFormat.format("dd-MM-yyyy", new Date(millisecond)).toString();

            if(CommonKotlin.Companion.is_debug()) e.printStackTrace();
        }}

        CommonKotlin.Companion.logging(reformattedStr);
        return reformattedStr;

    }


}// end main utils class