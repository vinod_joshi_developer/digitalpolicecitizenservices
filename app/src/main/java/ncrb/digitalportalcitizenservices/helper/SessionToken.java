package ncrb.digitalportalcitizenservices.helper;

public interface SessionToken {

    boolean isLoggedIn();

    void saveToken(String token);

    String getToken();

    void invalidate();

}// end api caller
