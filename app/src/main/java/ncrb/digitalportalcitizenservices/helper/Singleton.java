package ncrb.digitalportalcitizenservices.helper;

import java.security.PrivateKey;
import java.security.PublicKey;

public class Singleton {

    private static Singleton singleton = new Singleton( );

    public String session_token = "";
    public String LOGIN_ID = null;
    public String PS_STAFF_CD = null;

    public PublicKey cipherPublicKey = null;
    public PrivateKey cipherPrivateKey = null;

    /**
     *
     * A private Constructor prevents any other
     * class from instantiating.
     *
     */
    private Singleton(){ }

    /* Static 'instance' method */
    public static Singleton getInstance( ) {
        return singleton;
    }


}// end main singleton
