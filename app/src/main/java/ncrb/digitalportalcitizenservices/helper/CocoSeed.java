package ncrb.digitalportalcitizenservices.helper;

import android.util.Base64;

import java.security.KeyPair;
import java.security.KeyPairGenerator;

import javax.crypto.Cipher;

public class CocoSeed {

    public static final String cocoDe = "decode";
    public static final String cocoEn = "encode";

    Singleton singleton = Singleton.getInstance();

    public String seedMaker(String dataStr, String whatToDo) throws Exception {

        //Creating a Cipher object
        Cipher globalCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        if (whatToDo.equalsIgnoreCase(cocoEn)) {

            //Initializing a Cipher object
            globalCipher.init(Cipher.ENCRYPT_MODE, singleton.cipherPublicKey);

            //Add data to the cipher
            byte[] input = dataStr.getBytes();
            globalCipher.update(input);

            //encrypting the data
            byte[] cipherText = globalCipher.doFinal();

            String utf8 = new String(cipherText, "UTF8");

            String ciphered = Base64.encodeToString(utf8.getBytes(), Base64.DEFAULT);

            return ciphered;

        }

        if (whatToDo.equalsIgnoreCase(cocoDe)) {

            byte[] base64Byte = Base64.decode(dataStr, Base64.DEFAULT);

            String utf8 = new String(base64Byte, "UTF8");

            //Initializing the same cipher for decryption
            globalCipher.init(Cipher.DECRYPT_MODE, singleton.cipherPrivateKey);

            //Decrypting the text
            byte[] decipheredText = globalCipher.doFinal(utf8.getBytes());

            String deciphered = new String(decipheredText, "UTF8");

            return deciphered;
        }

        return "";
    }// seedMaker

    public  void keyGenerate() throws Exception {

        //Creating KeyPair generator object
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");

        //Initializing the key pair generator
        keyPairGen.initialize(2048);

        //Generate the pair of keys
        KeyPair pair = keyPairGen.generateKeyPair();

        //Getting the public key from the key pair
        singleton.cipherPublicKey = pair.getPublic();
        singleton.cipherPrivateKey = pair.getPrivate();

    }

}//end main
