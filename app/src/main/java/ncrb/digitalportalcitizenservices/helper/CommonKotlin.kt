package mha.ncrb.evidence.helper

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.media.ExifInterface
import android.media.MediaMetadataRetriever
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import ncrb.digitalportalcitizenservices.helper.Singleton
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue
import kotlin.math.roundToInt


class CommonKotlin {

    companion object {

        val is_debug:Boolean = true
        val is_demo:Boolean = false
        val internet_lost = "Internet connection lost. Please check."
        val NO_DATA = "Data not found on server."
        val STATUS_CODE_TRUE = "200"

        var singleton: Singleton = Singleton.getInstance()


        fun logging(msg:String){
            if(is_debug) println(msg)
        }

        fun getDisplayDateTime(dateTimeMySQL: String): String {
            try {
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
                val date = simpleDateFormat.parse(dateTimeMySQL)
                val convetDateFormat = SimpleDateFormat("dd MMMM yyyy hh:mm a", Locale.getDefault())
                return convetDateFormat.format(date)
            } catch (e: Exception) {
                return ""
            }
        }

//        fun isInternetAvailable(context: Context): Boolean {
//            var result = false
//            val connectivityManager =
//                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                val networkCapabilities = connectivityManager.activeNetwork ?: return false
//                val actNw =
//                    connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
//                result = when {
//                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
//                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
//                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
//                    else -> false
//                }
//            } else {
//                connectivityManager.run {
//                    connectivityManager.activeNetworkInfo?.run {
//                        result = when (type) {
//                            ConnectivityManager.TYPE_WIFI -> true
//                            ConnectivityManager.TYPE_MOBILE -> true
//                            ConnectivityManager.TYPE_ETHERNET -> true
//                            else -> false
//                        }
//
//                    }
//                }
//            }
//            return result
//        }

        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        //Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        //Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        //Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }


    }// companion object

}// end main