package ncrb.digitalportalcitizenservices.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import ncrb.digitalportalcitizenservices.R
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SplashScreen : AppCompatActivity() {

    private val SPLASH_TIME_OUT:Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

         // LoggerFactory SLF4J
        val LOG: Logger = LoggerFactory.getLogger(SplashScreen::class.java)
        LOG.info("Welcome to NCRB.")

        Handler(Looper.getMainLooper()).postDelayed({
            //Toast.makeText(this@SplashScreen, "LOOPER", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, Login::class.java))
            finish()
        }, SPLASH_TIME_OUT)

    }//onCreate

}// end main class

/*Auto format shortcut ctrl+alt+L */