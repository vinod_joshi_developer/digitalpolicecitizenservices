package ncrb.digitalportalcitizenservices.activity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Base64
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import mha.ncrb.evidence.helper.*
import ncrb.digitalportalcitizenservices.R
import ncrb.digitalportalcitizenservices.helper.*
import ncrb.digitalportalcitizenservices.pojo.ColumnNameOfTable
import ncrb.digitalportalcitizenservices.pojo.ServerSessionPoJo
import ncrb.digitalportalcitizenservices.pojo.UserRequestPoJo
import ncrb.digitalportalcitizenservices.pojo.UserResponsePoJo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : AppCompatActivity() {

    lateinit var appContext: Context
    var singleton: Singleton = Singleton.getInstance()

    var seed_user_credits: String = ""
    lateinit var et_email: EditText
    var token_error = "Fail to connect server. login token error."
    var server_issue = "Fail to connect server. server error after getting token."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        this.appContext = applicationContext

        if (CommonKotlin.is_demo) {
            val myIntent = Intent(this@Login, SplashScreen::class.java)
            startActivity(myIntent)
            finish()
        }

        //et_email = findViewById<EditText>(R.id.et_email)
        //val et_password = findViewById<EditText>(R.id.et_password)
        //val btn_login = findViewById<Button>(R.id.btn_login)

        chb_show_password.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                et_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                et_password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }

        // set on-click listener
        btn_login.setOnClickListener {

            if (et_email.text.toString().trim().isEmpty()) {
                et_email.error = "Required"
                Toast.makeText(applicationContext, "Username Required ", Toast.LENGTH_SHORT).show()
            } else if (et_password.text.toString().trim().isEmpty()) {
                et_password.error = "Required"
                Toast.makeText(applicationContext, "Password Required ", Toast.LENGTH_SHORT).show()
            } else {
                // check internet
                if (CommonKotlin.isOnline(this.applicationContext)) {

                    val objMap: Map<String, String> = mapOf<String, String>(
                        ColumnNameOfTable.LOGIN_ID to et_email.text.toString().trim()
                        , ColumnNameOfTable.LOGIN_PASSWORD to et_password.text.toString().trim()
                    )

                    pb_progress_login.visibility = View.VISIBLE
                    seed_user_credits = SeedWebData.seedDataParam(objMap)
                    this.startTokenSession()

                } else {
                    Toast.makeText(
                        applicationContext,
                        CommonKotlin.internet_lost,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }//setonclickklister
    }//onCreate

    fun startTokenSession() {

        val serverCreditsBase64 = String(Base64.decode(MCoCoRy.getCredits(), Base64.DEFAULT))

        val gson = Gson()
        val serverCredits: ServerSessionPoJo =
            gson.fromJson(serverCreditsBase64, ServerSessionPoJo::class.java)


        val apiCaller: ApiCaller = ServiceGenerator.createService<ApiCaller>(
            ApiCaller::class.java,
            serverCredits.server_user,
            serverCredits.server_pass
        )

        val call: Call<*> = apiCaller.apiSessionToken()

        call.enqueue {
            onResponse = {
                val serverSessionPoJo: ServerSessionPoJo = it.body() as ServerSessionPoJo
                if (serverSessionPoJo.STATUS_CODE == CommonKotlin.STATUS_CODE_TRUE) {
                    CommonKotlin.Companion.logging("token ${serverSessionPoJo.TOKEN}")
                    singleton.session_token = "Bearer " + serverSessionPoJo.TOKEN
                    sendToServer("Bearer " + serverSessionPoJo.TOKEN)
                } else dataNotFound()
            }
            onFailure = {
                pb_progress_login.visibility = View.GONE
                Toast.makeText(applicationContext, token_error, Toast.LENGTH_SHORT).show()
                CommonKotlin.Companion.logging(token_error)
            }
        }
    }// startTokenSession

    fun sendToServer(session_token: String) {

        val loginService: ApiCaller = ServiceGenerator.createService<ApiCaller>(
            ApiCaller::class.java,
            session_token
        )

        var userRequestPoJo = UserRequestPoJo()
        userRequestPoJo.SEED = seed_user_credits

        val call: Call<*> = loginService.apiLogin(userRequestPoJo)

        call.enqueue {
            onResponse = {
                val userResponsePoJo: UserResponsePoJo = it.body() as UserResponsePoJo
                afterServer(userResponsePoJo)
            }
            onFailure = {
                pb_progress_login.visibility = View.GONE
                Toast.makeText(applicationContext, server_issue, Toast.LENGTH_SHORT).show()
                CommonKotlin.logging(server_issue)
            }
        }
    }// startTokenSession

    fun afterServer(userResponsePoJo: UserResponsePoJo) {

        try {

            CommonKotlin.logging(userResponsePoJo.SEED)

            val decoded =
                String(Base64.decode(userResponsePoJo.SEED, Base64.DEFAULT), Charsets.UTF_8)

            CommonKotlin.logging(decoded)

            val gson = Gson()
            val webData: UserResponsePoJo = gson.fromJson(decoded, userResponsePoJo::class.java)

            if (webData.STATUS_CODE != CommonKotlin.STATUS_CODE_TRUE) {
                dataNotFound()
                return
            }

            this.singleton.LOGIN_ID = et_email.text.toString().trim()
            this.singleton.PS_STAFF_CD = webData.PS_STAFF_CD

            val myIntent = Intent(this@Login, SplashScreen::class.java)
            startActivity(myIntent)
            finish()

        } catch (e: Exception) {
            if (CommonKotlin.is_debug) e.printStackTrace()
            dataNotFound()
        }

    }

    fun dataNotFound() {
        CommonKotlin.logging(CommonKotlin.NO_DATA);
        Toast.makeText(applicationContext, CommonKotlin.NO_DATA, Toast.LENGTH_LONG).show()
    }

    fun <T> Call<T>.enqueue(callback: CallBackLogin<T>.() -> Unit) {
        val callBackKt = CallBackLogin<T>()
        callback.invoke(callBackKt)
        this.enqueue(callBackKt)
    }

    class CallBackLogin<T> : Callback<T> {

        var onResponse: ((Response<T>) -> Unit)? = null
        var onFailure: ((t: Throwable?) -> Unit)? = null

        override fun onFailure(call: Call<T>, t: Throwable) {
            onFailure?.invoke(t)
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            onResponse?.invoke(response)
        }
    }

}// end main class
